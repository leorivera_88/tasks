<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
})->middleware('guest');

Auth::routes();

// Home page for specific idUser
Route::get('/home/{idUser?}', 'HomeController@index')->name('home');
// users module
Route::get('/users', 'UserController@index')->name('users');

// Home page api
Route::get('/api/task/{idUser?}', 'TaskController@getAllTasks');
Route::post('/api/task/{idUser?}', 'TaskController@createTask');
Route::put('/api/task', 'TaskController@update');
Route::delete('/api/task/{id_task}', 'TaskController@delete');

// User api
Route::get('/api/user', 'UserController@getAllUsers');
Route::post('/api/user', 'UserController@create');
Route::put('/api/user', 'UserController@update');
Route::delete('/api/user/{id_user}', 'UserController@delete');

// Download files
Route::get('/api/download/csv/{idUser?}', 'DownloadController@downloadCSV');
Route::get('/api/download/xml/{idUser?}', 'DownloadController@downloadXML');
