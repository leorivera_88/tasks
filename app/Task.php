<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'task';

    protected $primaryKey = "id_task";

    protected $fillable = [
        'description', 'state', 'id_user'
    ];

    public function user(){
		return $this->hasOne('User','id_user','id_user');
	}
}
