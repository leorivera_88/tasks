<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exporter\Handler;
use Exporter\Writer\CsvWriter;
use Exporter\Writer\XmlWriter;
use Exporter\Source\ArraySourceIterator;

class DownloadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function downloadCSV($idUser = null){
    	$this->generateFile("csv", $idUser);
    }

    public function downloadXML($idUser = null){
    	$this->generateFile("xml", $idUser);
    }

    // Use the package "sonata-project/exporter" Lightweight Exporter library
    public function generateFile($format, $idUser){
    	$taskController = new TaskController();
    	$data =  $taskController->getAllTasksArray($idUser);

        // Filename
        $filename = 'test-file';

        // Set Content-Type
        $content_type = 'text/'.$format;

        // Location to Export this to
        $export_to = 'php://output';

        // We're adding an Array to the Array Source Iterator
        $source = new ArraySourceIterator($data);

    	// Prepare the writer
		if($format == "csv"){
			$writer = new CsvWriter($export_to);
		}else{
			$writer = new XmlWriter($export_to);
		}

		// Set the right headers
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename=file.' . $format . ';');

        // Export the data
		Handler::create($source, $writer)->export();
    }
}
