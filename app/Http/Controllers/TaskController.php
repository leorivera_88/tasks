<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;

class TaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    // get All tasks from specific user in an Array
    public function getAllTasksArray($idUser = null){
        $idUser = ($idUser && \Auth::user()->role == "admin") ? $idUser : \Auth::user()->id_user;
        $data = array();
        $results = Task::where('id_user', $idUser)->get();
        foreach($results as $res){
            $data[]=array(
                'id_task' => $res->id_task,
                'description' => $res->description,
                'state' => $res->state,
                'created_at' => date("Y-m-d H:i:s", strtotime($res->created_at)),
                'updated_at' => date("Y-m-d H:i:s", strtotime($res->updated_at)),
            );
        }
        return $data;
    }

    // GET /api/task/{idUser?}
    public function getAllTasks($idUser = null){
        $data = $this->getAllTasksArray($idUser);
        $response = array("success"=>"true","data"=>$data);
        return \Response::json($response);
    }

    // POST /api/task/{idUser?}
    public function createTask(Request $request, $idUser = null){
        $idUser = ($idUser && \Auth::user()->role == "admin") ? $idUser : \Auth::user()->id_user;
        Task::create([
            'state' => $request->input("state"),
            'description' => $request->input("description"),
            'id_user' => $idUser
            ]);
    }

    // PUT /api/task
    public function update(Request $request){
    	$task = Task::find($request->input("id"));
        if($task){
        	$task->state = $request->input("state");
        	$task->description = $request->input("description");
        	$task->save();
        }
    }

    // DELETE /api/task/{id_task}
    public function delete($id_task){
    	Task::destroy($id_task);
    }
}
