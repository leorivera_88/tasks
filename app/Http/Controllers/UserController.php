<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    // get the view for User module
    public function index(){
    	return view("users");
    }

    // GET /api/user
    public function getAllUsers(){
        $data = array();
        $results = User::all();
        foreach($results as $res){
            $data[]=array(
                'id_user' => $res->id_user,
                'firstName' => $res->first_name,
                'lastName' => $res->last_name,
                'email' => $res->email,
                'role' => $res->role,
                'completeName' => $res->completeName()
            );
        }
        $response = array("success"=>"true","data"=>$data);
        return \Response::json($response);
    }

    // POST /api/user
    public function create(Request $request){
        $this->validate($request, [
            'email' => 'required|string|email|max:255|unique:user',
            'password' => 'required|string|min:6|confirmed'
        ]);

    	User::create([
    		'first_name' => $request->input("firstName"),
    		'last_name' => $request->input("lastName"),
    		'email' => $request->input("email"),
    		'role' => $request->input("role"),
    		'password' => bcrypt($request->input("password"))
    		]);
    }

    // PUT /api/user
    public function update(Request $request){
    	$user = User::find($request->input("id_user"));
    	if($user){
            //validate password requirements only if the password is not blank
            if(strlen(trim($request->input("password"))) > 0){
                $this->validate($request, ['password' => 'required|string|min:6|confirmed']);
                $user->password = bcrypt($request->input("password"));
            }
	    	$user->first_name = $request->input("firstName");
	    	$user->last_name = $request->input("lastName");
	    	$user->role = $request->input("role");
	    	$user->save();
	    }
    }

    // DELETE /api/user/{id_user}
    public function delete($id_user){
    	User::destroy($id_user);
    }
}
