<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'user';

    protected $primaryKey = "id_user";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // get all task by this user
    public function task(){
        return $this->hasMany('Task','id_user','id_user');
    }

    // get the complete name of the user
    public function completeName(){
        if(strlen(trim($this->first_name." ".$this->last_name)) == 0){
            // the user has no name setting a default one
            return "[No name]";
        }else{
            //concatenate the first and last name
            return $this->first_name." ".$this->last_name;
        }
    }
}
