angular.module('angularApp')
.controller('homeController', function($scope, $http, $window, $location) {
    //initialie variables
    $scope.myTasks  = {};
    $scope.users = [];
    $scope.taskTitle = "My tasks";
    var lastIndex = window.location.href.lastIndexOf("?user=");
    $scope.idUser = (lastIndex > 0 ) ? window.location.href.substring(lastIndex + 6) : 0;

    // initialize "New" state for new tasks
    $scope.newTask = function(){
      $scope.task = { state: "New"};
      document.getElementById("state").disabled = true;
    }

    // retrive all tasks for the specified idUser
    $scope.getTaskList = function(){
      $scope.additionalPath = ($scope.idUser == undefined) ? "" : $scope.idUser;
      // GET /api/task/{idUser?}
      $http.get("/api/task/" + $scope.additionalPath)
        .then(function (response) {
        if(response.data.success == "true"){
          $scope.myTasks = response.data.data
        }
      });
    }

    // create new task or update task function
    $scope.save = function(){
      $scope.additionalPath = ($scope.idUser == undefined) ? "" : $scope.idUser;
      if(document.getElementById("state").disabled){
        // create new task for idUser
        // POST /api/task/{idUser?}
        $http.post('/api/task/' + $scope.additionalPath, $scope.task)
        .then(function (response) {
            $scope.getTaskList();
        });
      }else{
        // update task
        // PUT /api/task/
        $http.put('/api/task/', $scope.task)
        .then(function (response) {
            $scope.getTaskList();
        });
      }
    }

    // preparing "edit task" popup form with all the information related with this specific task
    $scope.edit = function(task){
      $scope.task = { state: task.state, description: task.description, id: task.id_task};
      document.getElementById("state").disabled = false;
    }

    // delete task with the parameter id_task
    $scope.delete = function(id_task){
      // DELETE /api/task/{id_task}
      $http.delete('/api/task/' + id_task)
      .then(function (response) {
          $scope.getTaskList();
      });
    }

    // color the badges depending of the status task
    $scope.badgeState = function(state){
      if(state == "In process" ) return "label label-info";
      else if(state == "Finished") return "label label-success";
      else return "label label-warning";
    }

    // get all users list
    $scope.getAllUsers = function(){
      // GET /api/user
      $http.get('/api/user')
      .then(function (response) {
        if(response.data.success == "true"){
          response.data.data.forEach(function(row){
            // the title of the task list
            if($scope.idUser == row.id_user){
              $scope.taskTitle = row.completeName;
            }
            // use this array to fill the list options to change user
            $scope.users.push({
              id_user: row.id_user,
              completeName: row.completeName
            });
          });
        }
      });
    }

    // access tasks for specified id_user
    $scope.toUserHome = function(id_user){
      // redirect to change user
      $window.location.href = '/home?user=' + id_user;
    }

    // Export task list in a file
    $scope.download = function(type){
      $http.get("/api/download/" +  type + "/" + $scope.idUser)
        .then(function (response) {
          var anchor = angular.element('<a/>');
          anchor.css({display: 'none'}); // Make sure it's not visible
          angular.element(document.body).append(anchor); // Attach to document

          anchor.attr({
              href: 'data:attachment/' + type + ';charset=utf-8,' + encodeURI(response.data),
              target: '_blank',
              download: $scope.taskTitle + '.' + type
          })[0].click();

          anchor.remove(); // Clean it up afterwards
      });
    }

    // initializators
    $scope.newTask();
    $scope.getAllUsers();
    $scope.getTaskList();
});