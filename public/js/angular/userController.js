angular.module('angularApp')
.controller('userController', function($scope, $http) {
    // initializing default variables
    $scope.user  = {};
    $scope.users = {};
    $scope.new = true;
    $scope.validation = {};
   
    // cleaning form for new user
    $scope.newUser = function(){
      $scope.user = { firstName: "", lastName: "", email: "", role: "regular", password: "" };
      $scope.new = true;
      document.getElementById("email").disabled = false;
    }

    // create new user or update user
    $scope.save = function(){
      $scope.validation = {};
      if($scope.new){
        // creating new user
        // POST /api/user
        $http.post('/api/user', $scope.user)
        .then(function (response) {
            //refresh list for users
            $scope.getAllUsers();
            $('#basicModal').modal('hide');
        }, function (response){
          // a validation failed
          $scope.errorMessage(response);
        });
      }else{
        // updating user information
        // PUT /api/user
        $http.put('/api/user/', $scope.user)
        .then(function (response) {
            //refresh list for users
            $scope.getAllUsers();
            $('#basicModal').modal('hide');
        }, function (response){
          // a validation failed
          $scope.errorMessage(response);
        });
      }
    }

    // display validation message in the new/edit user form
    $scope.errorMessage = function(response){
      if(response.data.email){
          $scope.validation.email = response.data.email[0];
      }else if(response.data.password){
        $scope.validation.password = response.data.password[0];
      }
    }

    // preparing "edit user" popup form with all the information related with this specific user
    $scope.edit = function(user){
      $scope.user = { firstName: user.firstName, lastName: user.lastName, email: user.email, role: user.role, id_user: user.id_user, password: "" };
      $scope.new = false;
      document.getElementById("email").disabled = true;
    }

    // delete user with the parameter id_user
    $scope.delete = function(id_user){
      // DELETE /api/user/{id_user}
      $http.delete('/api/user/' + id_user)
      .then(function (response) {
          //refresh list for users
          $scope.getAllUsers();
      });
    }

    // retrive the complete list for users
    $scope.getAllUsers = function(){
      // GET /api/user
      $http.get('/api/user')
      .then(function (response) {
        if(response.data.success == "true"){
          $scope.users = response.data.data
        }
      });
    }

    // initializator
    $scope.getAllUsers();
});