<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user')->insert([
            'first_name' => "Leonardo",
            'last_name' => "Rivera",
            'email' => 'leorivera88@gmail.com',
            'role' => 'admin',
            'password' => bcrypt('123456')
        ]);
    }
}
