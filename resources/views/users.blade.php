@extends('layouts.app')

@section('content')
<div class="container" ng-controller="userController" ng-cloak>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>Users</h2>
                </div>

                <div class="panel-body">
                    <a href="#" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#basicModal" ng-click="newUser()">Add new</a>
                    <br /><br />
                        <table class="table">
                            <tr>
                                <th>First name</th>
                                <th>Last name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th></th>
                            </tr>
                            <tr ng-repeat="user in users">
                                <td ng-bind="user.firstName"></td>
                                <td ng-bind="user.lastName"></td>
                                <td ng-bind="user.email"></td>
                                <td ng-bind="user.role"></td>
                                <td >
                                    <a href="#" ng-click="edit(user)" data-toggle="modal" data-target="#basicModal">Edit</a>
                                    <a href="#" ng-click="delete(user.id_user)">Delete</a>
                                </td>
                            </tr>
                        </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">User</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
                <div class="col-s6">
                    <label class="control-label" >First name</label>
                </div>
                <div class="col-s6">
                    <input type="text" class="form-control" ng-model="user.firstName" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-s6">
                    <label class="control-label" >Last name</label>
                </div>
                <div class="col-s6">
                    <input type="text" class="form-control" ng-model="user.lastName" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-s6">
                    <label class="control-label" >Email</label>
                </div>
                <div class="col-s6">
                    <input type="text" class="form-control" ng-model="user.email" id="email" />
                    <span class="help-block">
                        <strong ng-bind="validation.email"></strong>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-s6">
                    <label class="control-label" >Password</label>
                </div>
                <div class="col-s6">
                    <input type="password" id="password" class="form-control" ng-model="user.password" />
                    <span class="help-block">
                        <strong ng-bind="validation.password"></strong>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-s6">
                    <label class="control-label" >Confirm Password</label>
                </div>
                <div class="col-s6">
                    <input id="password-confirm" type="password" class="form-control" ng-model="user.password_confirmation">
                </div>
            </div>
            <div class="form-group">
                <div class="col-s6">
                    <label class="control-label">Role</label>
                </div>
                <div class="col-s6">
                    <select id="state" class="form-control" ng-model="user.role">
                        <option value="admin">Admin</option>
                        <option value="regular">Regular</option>
                    </select>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" ng-click="save()">Save changes</button>
          </div>
        </div>
      </div>
    </div>

</div>
@endsection


@section('scripts')
<script src="js/angular/userController.js"></script>
@endsection