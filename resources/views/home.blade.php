@extends('layouts.app')

@section('content')
<div class="container" ng-controller="homeController">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 ng-bind="taskTitle"></h2>
                    @if(Auth::user()->role == "admin")
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Change user
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li ng-repeat="user in users"><a href="" ng-bind="user.completeName" ng-click="toUserHome(user.id_user)"></a></li>
                            </ul>
                        </div>
                    @endif
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <a href="#" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#basicModal" ng-click="newTask()">Add new</a>
                        </div>
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-3">
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    Export
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                    <li><a href="" ng-click="download('csv')" target="_blank">CSV</a></li>
                                    <li><a href="" ng-click="download('xml')" target="_blank">XML</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <br /><br />
                    <div class="row">
                        <div class="col-sm-6" ng-repeat="task in myTasks">
                            <div class="thumbnail">
                                <div class="caption">
                                    <span ng-class="badgeState(task.state)" ng-bind="task.state"></span>
                                    <br /><br />
                                    <div>
                                    <p ng-bind="task.description"></p>
                                    </div>
                                    <br />
                                    <div>
                                        <small>Created date: <span ng-bind="task.created_at"></span></small>
                                    </div>
                                    <div>
                                        <small>Last updated: <span ng-bind="task.updated_at"></span></small>
                                    </div>
                                    <br />
                                    <div>
                                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#basicModal" ng-click="edit(task)">Edit</a>
                                        <a href="#" class="btn btn-default" role="button" ng-click="delete(task.id_task)">Delete</a>
                                    </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Task</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
                <div class="col-s6">
                    <label class="control-label">State</label>
                </div>
                <div class="col-s6">
                    <select id="state" class="form-control" ng-model="task.state">
                        <option value="New">New</option>
                        <option value="In process">In process</option>
                        <option value="Finished">Finished</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-s6">
                    <label class="control-label" >Description</label>
                </div>
                <div class="col-s6">
                    <textarea class="form-control" id="description" ng-model="task.description"></textarea>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="save()">Save changes</button>
          </div>
        </div>
      </div>
    </div>

</div>
@endsection


@section('scripts')
<script src="/js/angular/homeController.js"></script>
@endsection